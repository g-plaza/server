# gplaza-server

## Base express starter

- Using [typescript-express-starter](https://github.com/ljlm0402/typescript-express-starter) mongoose template.

## Install

- git clone
- cd xxx
- npm install
- Development: npm run dev => http://localhost:3000/api-docs/
- Production: npm run start

## TODO

- Latitud y longitud desde nombre de calle. Opencalc migrationPrueba2 adaptado.Adaptar los cambios a los cambios de columnas del opencalc. quitar los que no tengan todos los datos
- v0.0.1
- Que las comunidades vayan probando.

- Hacer el detalle de un don y de una necesidad
- Que en el listado de un don o necesidad al clicar en uno concreto aparezcan los demas usuarios que tienen ese don o necesidad.
- Subir la aplicacionzilla a nuestra infraestructura con dominio propio.
- v0.0.2
- Aqui tendriamos la aplicacion funcional. Tema introduccion de datos, listado y busqueda.
- Dejamos un tiempo para ir solucionando las cosas que hayan salido.
- Subir frontend: https://aitorjs.gitlab.io/gplaza-client

- Probar con los otros dones y talentos que estan en el opencalc
- Terminar con los tests unitarios que quedan.

  [DONE]

- Terminar el test unitario que queda para processNecesidades.
- Reimplementar diccionario.
- Probar con los dones y necesidades de donosti.
- Cambiar metadata a dons_metadata + meter modelo necesidads_metadata.
- Buscar un punto donde haya datos mas cerca que ha 3km. thunderclient de community dones 1km from bulebard donosti
- Subir a heroku.
- Meter indices de texto y geojson + probar diferentes queryes incluida la de por distancia en uno de donosti. usar los postman por facilidad y rapidez. Usado thunder client. NO puede exportar los requests.
- Enlazar la nueva api + hacerle los cambios necesarios
- crear GET categories (https://gplaza-s.herokuapp.com/categories) y GET communities (https://gplaza-s.herokuapp.com/communities) que al cargar la app ya tenga la data de categorioes y communities usando estructura de respuesta de App.vue:99-117 + para multi-comunidad
- hacerle los cambios necesarios:buscador categorias, buscador por distancia, buscador libre, parte de don

[:bug]

- [frontend] si en el buscador libre se le da al intro, mete un "?search={texto-introducido}" que hace que si pasas a otra comunidad mediante el selectro, no funcione.

[BACKLOG]

- Nuevo diseño de dones y necesidades con tailwind responsive first.
- Migrador funcione para editar y eliminar dones y necesidades.

## Puntos

- 43.3170546, -1.9775704 => tercer puente de donosti, cerca de la estacion de autobuses
- 43.3172119368173,-1.97544130496173 => cerca de tabakalera. A 240m de 43.3418968,-1.8073704
- 43.322181, -1.984496 => bulebard de donosti

## Tips

- De latlong a direccion: https://www.latlong.net/Show-Latitude-Longitude.html

## Estructura de un don:

```
{
  _id: new ObjectId("6331fe2617d6733c9061b075"),
  name: 'enseñar euskera',
  necesidades: 1,
  dones: 1,
  categories: [
    { _id: new ObjectId("633315b6973160bf2ee3a516"), name: 'lo' },
    { _id: new ObjectId("633315ae973160bf2ee3a3b0"), name: 'jan' }
  ],
  marker: '',
  community: {
    _id: new ObjectId("63309ce817d6733c900c8368"),
    name: 'donosti2',
    center: '43.3172119368173,-1.97544130496173'
  },
  users: [
    {
      _id: new ObjectId("633208a017d6733c90645dee"),
      username: 'a',
      email: 'a@a.com'
    },
    {
      _id: new ObjectId("63331437973160bf2ee3397e"),
      username: 'b',
      email: 'b@b.com'
    }
  ]
}
```

## Distancia entre dos puntos de leaflet

https://stackoverflow.com/questions/67918593/how-to-calculate-the-distance-between-two-markers-in-leaflet-using-distanceto/67918989#67918989

## Nueva estructura buscable por texto, categorias y distancia con los populates necesarios

- Estructura colecciones de mongo

dons_point:
(campos para la busqueda)

- name: "xxxx"
- community: ObjectId("xxxx")
- categories: [ObjectId("xxxx"), ObjectId("xxxx")]
- location: {
  "type" : "Point",
  "coordinates" : [
  43.3172119368173,
  -1.97544130496173
  ]}
  (FIN campos para la busqueda)
- users: [ObjectId("xxxx"), ObjectId("xxxx")] => para poder sacar un area con todos los que tienen ese dato. al clicar en su avatar vaya a su punto
- position: 1
- metadata: ObjectId("xxxx")

dons_metadata:

- necesidades: 0
- dones: 0
- locations: [[lat,lang], [lat,lang]] => para pintar los puntos del don en el mapa
- descriptions: ["textoDescript1", "textoDescript2"] => para pintar las descripciones de cada don sin volverse loco

necesidades_point:
(como dons_point)

necesidades_metadata:
(como dons_metadata)

communities:

- name: "xxx"
- center: [lat,lang]

categories:

- name: "xxx"

users:

- name: "xxx"
- contacts_ways: [
  {
  "type" : "telegram",
  "data" : "@pepe"
  }
  ]
- center: [lat,lang]

### Datos para testando la bd mongo para los tests

#### categories

/_ 1 _/
{
"\_id" : ObjectId("6349eb2bd1c1dead2d522ae5"),
"name" : "lo"
}

/_ 2 _/
{
"\_id" : ObjectId("634c1976d1c1dead2de0a7dd"),
"name" : "lagunak"
}

/_ 3 _/
{
"\_id" : ObjectId("634c1983d1c1dead2de0ab49"),
"name" : "jan"
}

#### communities

/_ 1 _/
{
"\_id" : ObjectId("6346055bf1d3ca76e73f01b5"),
"name" : "Donosti",
"center" : "43.3418969,-1.8073705",
"\_\_v" : 0
}

/_ 2 _/
{
"\_id" : ObjectId("6349ea93d1c1dead2d520164"),
"name" : "Hernani",
"center" : "43.3418969,-1.8073705"
}

#### dons

(empty)

#### dons_metadata

(empty)

#### necesidads

(empty)

#### necesidads_metadata

(empty)

#### users

/_ 1 _/
{
"\_id" : ObjectId("6346055bf1d3ca76e73f01b7"),
"name" : "Alice",
"username" : "Alice_ehfhdqqem",
"email" : "_oysikkq7j@gplaza.eus",
"contact_ways" : [
{
"type" : "telegram",
"data" : "@alice"
}
],
"center" : "43.3226904864312,-1.98216234959986",
"\_\_v" : 0
}

/_ 2 _/
{
"\_id" : ObjectId("6346055bf1d3ca76e73f01b9"),
"name" : "Paco",
"username" : "Paco_6pwz0cj6f",
"email" : "_s97mcrzpx@gplaza.eus",
"contact_ways" : [
{
"type" : "telegram",
"data" : "@paco"
}
],
"center" : "1,2",
"\_\_v" : 0
}

/_ 3 _/
{
"\_id" : ObjectId("6346055cf1d3ca76e73f01bc"),
"name" : "Egoitz",
"username" : "Egoitz_zyksjfw35",
"email" : "_6vxptl9el@gplaza.eus",
"contact_ways" : [
{
"type" : "telegram",
"data" : "@egoitz"
}
],
"center" : "43.3172119368173,-1.97544130496173",
"\_\_v" : 0
}
