import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import CategoriesController from '@/controllers/categories.controller';

class CategoriesRoute implements Routes {
  public path = '/categories';
  public router = Router();
  public categoriesController = new CategoriesController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.categoriesController.getCategories);
  }
}

export default CategoriesRoute;
