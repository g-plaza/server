import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import DonesController from '@/controllers/dones.controller';

class DonesRoute implements Routes {
  public path = '/search';
  public router = Router();
  public donesController = new DonesController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}`, this.donesController.getDones);
  }
}

export default DonesRoute;
