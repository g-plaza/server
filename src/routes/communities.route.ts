import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import CommunitiesController from '@/controllers/communities.controller';

class CommunitiesRoute implements Routes {
  public path = '/communities';
  public router = Router();
  public communitiesController = new CommunitiesController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.communitiesController.getCommunities);
  }
}

export default CommunitiesRoute;
