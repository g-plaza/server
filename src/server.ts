import App from '@/app';
import AuthRoute from '@routes/auth.route';
import IndexRoute from '@routes/index.route';
import UsersRoute from '@routes/users.route';
import DonessRoute from '@routes/dones.route';
import CategoriesRoute from '@routes/categories.route';
import CommunitiesRoute from '@routes/communities.route';
import validateEnv from '@utils/validateEnv';

validateEnv();

const app = new App([new IndexRoute(), new AuthRoute(), new UsersRoute(), new DonessRoute(), new CategoriesRoute(), new CommunitiesRoute()]);
app.listen();
