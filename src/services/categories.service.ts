import { CategoryObj } from '@/controllers/categories.controller';
import { Category } from '@interfaces/category.interface';
import categoryModel from '@models/category.model';

class CategoryService {
  public categories = categoryModel;

  public async findAllCategories(): Promise<any> {
    const categories: Category[] = await this.categories.find();

    let categoriesObj = {};
    categories.map((c: CategoryObj<Category>) => categoriesObj[c.name] = c._id.toString())

    return categoriesObj;
  }
}

export default CategoryService;
