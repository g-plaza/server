import { CategoryObj } from '@/controllers/categories.controller';
import { Category } from '@/interfaces/category.interface';
import { Community } from '@interfaces/community.interface';
import communityModel from '@models/community.model';

type CategoryObj2<T> = {
  [K in keyof T]: { _id: K, name: T[K], center: string }
}[keyof T]

class CommunityService {
  public communities = communityModel;

  public async findAllCommunities(): Promise<CategoryObj2<Category>> {
    const communities: Community[] = await this.communities.find();

    let communitiesObj: CategoryObj2<Category> | any = {};
    communities.map((c: CategoryObj2<Category>) => communitiesObj[c.name] = {
      _id: c._id.toString(),
      center: c.center
    })

    return communitiesObj;
  }
}

export default CommunityService;
