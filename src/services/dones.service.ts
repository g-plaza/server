import { Don } from '@interfaces/dones.interface';
import donModel from '@models/don.model';
import necesidadModel from '@models/necesidad.model';

/* curl - X 'POST' \
'http://localhost:3000/dones' \
-H 'accept: application/json' \
-H 'Content-Type: application/json' \
-d '{
"type": "dones",
  "community": "6366cd521ee7b4927fb2e220"
} ' */

class DonesService {
  // public dones = donModel;

  // donosti center es 43.3418969, -1.8073705 (tercer puente)
  // punto en 43.3172119368173, -1.97544130496173 (de egoitz, plaza delante tabakalera)
  // entra en minDistance:0 y maxDistance: 238
  // el punto esta a 238 metros del centro de donosti que es el tercer puente
  // 43.3076859093751, -2.8724959418086  es mutriku y de txurruka
  // 43.3190879726024, -2.01610018621829 igeldo herria
  // 43.3222903119054, -1.95233080545868 viviendas encima de los viveros de ulia
  // https://www.google.es/maps/dir//43.319088,-2.0161002/@43.3099719,-2.0017581,13.5z/data=!4m2!4m1!3e0
  public async findAllDones(search: any, freeText: string, type: string): Promise<any[]> {
    console.log('search! on findalldones', search);
    // const donostiCenter = [43.3170546, -1.9775704];
    /* const search = {
            categories: { $in: ["633315ae973160bf2ee3a3b0"] },
            community: { $in: ["63309ce817d6733c900c8368"] },
            // db.getCollection('dons').createIndex( { name: "text" } )
            // $text: { $search: "\"euskera\"" },
            // db.getCollection('dons').createIndex( { location: "2dsphere" } )
            location: {
                $near: {
                    $geometry: {
                        type: "Point", coordinates: donostiCenter
                    },
                    $minDistance: 1,
                    $maxDistance: 2870
                }


            }
        } */

    let dones = [];
    let maxDistance = undefined;
    
    if (type === "dones") {
      dones = await donModel.find(search).populate('community').populate('categories').populate('users').populate('metadata');
    } else if (type === "necesidades") {
      dones = await necesidadModel.find(search).populate('community').populate('categories').populate('users').populate('metadata');
    }
    // const freeText = search['$text']['$search'] ? search['$text']['$search'] : undefined;
    // const freeText = search.hasOwnProperty("$text") ? search['$text']['$search'] : undefined;

    if (search['location'] !== undefined) {
      console.log("dones", search['location']['$near']['$maxDistance'])
      // maxDistance = search['location']['maxDistance'];
      maxDistance = search['location']['$near']['$maxDistance'];
    }
    console.log("AQUI", maxDistance, freeText)
    // distance search is not compatible with the search
    if (maxDistance != undefined && parseInt(maxDistance) !== 0 && freeText != undefined && freeText != '') {
      console.log('freeText with distance', freeText, dones);
      dones = dones.filter(res => {
        return res.name.startsWith(freeText) || res.name.includes(freeText);
      });
    }

    /* console.log(
      'dones',
      this.uniqByKeepLast(dones, it => it.name),
      dones.length,
      dones,
    ); */
    return this.uniqByKeepLast(dones, it => it.name);
  }

  private uniqByKeepLast(data: any, key: any) {
    return [...new Map(data.map(x => [key(x), x])).values()];
  }
}

export default DonesService;
