import { model, Schema, Document } from 'mongoose';
import communityModel from './community.model';
import userModel from './users.model';
import categoryModel from './category.model';
import necesidadMetadataModel from './necesidads-metadata.model';
import { Necesidad } from '@/interfaces/necesidades.interface';

const necesidadSchema: Schema = new Schema({
  name: String,
  community: {
    type: Schema.Types.ObjectId,
    ref: communityModel,
  },
  categories: [
    {
      type: Schema.Types.ObjectId,
      ref: categoryModel,
    },
  ],
  location: Object,
  users: [
    {
      type: Schema.Types.ObjectId,
      ref: userModel,
    },
  ],
  position: Number,
  metadata: {
    type: Schema.Types.ObjectId,
    ref: necesidadMetadataModel,
  },
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
});

const necesidadModel = model<Necesidad & Document>('Necesidad', necesidadSchema);

export default necesidadModel;
