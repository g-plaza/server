import { model, Schema, Document } from 'mongoose';
import { Don } from '@/interfaces/dones.interface';
import communityModel from './community.model';
import userModel from './users.model';
import categoryModel from './category.model';
import donMetadataModel from './dons-metadata.model';

const donSchema: Schema = new Schema({
  name: String,
  community: {
    type: Schema.Types.ObjectId,
    ref: communityModel,
  },
  categories: [
    {
      type: Schema.Types.ObjectId,
      ref: categoryModel,
    },
  ],
  location: Object,
  users: [
    {
      type: Schema.Types.ObjectId,
      ref: userModel,
    },
  ],
  position: Number,
  metadata: {
    type: Schema.Types.ObjectId,
    ref: donMetadataModel,
  },
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
});

const donModel = model<Don & Document>('Don', donSchema);

export default donModel;
