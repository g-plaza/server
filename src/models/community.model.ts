import { model, Schema, Document } from 'mongoose';
import { Community } from '@/interfaces/community.interface';

const communitySchema: Schema = new Schema({
  name: String,
  center: String,
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
});

const communityModel = model<Community & Document>('Community', communitySchema);

export default communityModel;
