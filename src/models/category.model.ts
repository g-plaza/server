import { model, Schema, Document } from 'mongoose';
import { Category } from '@/interfaces/category.interface';

const categorySchema: Schema = new Schema({
  name: String,
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
});

const categoryModel = model<Category & Document>('Category', categorySchema);

export default categoryModel;
