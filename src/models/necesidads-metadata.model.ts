import { model, Schema, Document } from 'mongoose';
import { Metadata } from '@/interfaces/metadata.interface';

const necesidadsMetadataSchema: Schema = new Schema({
    necesidades: Number,
    dones: Number,
    /* locations: Array<String>, */
    descriptions: Array<String>,
    createdAt: {
        type: Date,
        required: false,
    },
    updatedAt: {
        type: Date,
        required: false,
    },
});

const necesidadsMetadataModel = model<Metadata & Document>('necesidads_metadata', necesidadsMetadataSchema);

export default necesidadsMetadataModel;
