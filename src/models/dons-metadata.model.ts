import { model, Schema, Document } from 'mongoose';
import { Metadata } from '@/interfaces/metadata.interface';

const donsMetadataSchema: Schema = new Schema({
  necesidades: Number,
  dones: Number,
  /* locations: Array<String>, */
  descriptions: Array<String>,
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
});

const donsMetadataModel = model<Metadata & Document>('dons_metadata', donsMetadataSchema);

export default donsMetadataModel;
