/*
 * LOGICA: OBSOLETA!
 *
 * 1. Primero crear un objecto con los datos que vienen del odt/opencalc: decorateObject
 * 2. Despues, une todos los objetos con el mismo nombre en uno modificando done, users y locations: unirObject.
 * 3. Mete los datos. Si la necesidad ya existe la actualiza con los datos que res si es nueva, la añade: sendDb.
 * 4. Crear un objeto con todos los puntos que se quieren añadir. Esto es necesario realizarlo porque no se ha conseguido poder
 * la busqueda por geoposition desde un array de locations. Funciona con la key location y dentro un punto. Se crea
 * un  objeto con todos los puntos, duplicados incluidos: generatePoints.
 * 5. Rellena la tabla de points por necesidad/don usando los datos devueltos por en el punto anterior: sendPointsToDb
 *
 */

import { connect, disconnect } from 'mongoose';
import communityModel from '@models/community.model';
import categoryModel from '@models/category.model';
import donModel from '@models/don.model';
import necesidadModel from '@models/necesidad.model';
import donMetadataModel from '@/models/dons-metadata.model';
import necesidadMetadataModel from '@/models/necesidads-metadata.model';
import userModel from '@models/users.model';
import excelToJson from 'convert-excel-to-json';
import { Community } from '@/interfaces/community.interface';
import { Category } from '@/interfaces/category.interface';
import { Don } from '@/interfaces/dones.interface';
// import { Necesidad } from '@/interfaces/necesidad.interface';
import { HttpException } from '@/exceptions/HttpException';
import { DB_USER, DB_PASS, DB_HOST, DB_DATABASE, NODE_ENV } from '@config';
import { User } from '@/interfaces/users.interface';
import fetch from 'node-fetch';


// TODO
// - Pensar como meter las comunidades y usuarios necesarios que se necesitan para hacer
// andar los tests

interface Seed {
  name: string;
  community: Community;
  categories: Category[];
  location: {
    type: 'point';
    coordinates: Array<number>;
  };
  users: User[];
  metadata: string;
}

// interface Don extends Seed { }
type Necesidad = Seed;

type TSeed = Don | Necesidad;

export interface Migrator {
  dones: Don[];
  necesidades: Necesidad[];
}

export class Migrator {
  synonyms: any[] = [{
    "necesidades": "aprender",
    "dones": "enseñar"
  },
  {
    "necesidades": "eskutxado",
    "dones": "eskutxar"
  }];

  constructor() { }

  // TODO: new() donde se abstrae la logica de los pasos del Migrator ???
  // asi decorateObject(), processCommunity()... puede ser private

  async decorateObject(seed, type: string, latLongs) {
    const pb = seed.map(async (s: Don) => {
      // Necesidad | Don
      const community = s['D'];
      const name = s['A'];
      const description = s['F'] || undefined;
      const username = s['G'];
      const keyUsername = username.replace(" ", "-");
      // TODO: entran en minusculas y sin acentos.
      let categories = s['B'];
      categories = categories.split(',').map(e => e.trim());
      let necesidades = 0;
      let dones = 0;

      /* const user = await userModel.find({ name: username });
      const [lat, long] = user[0].center.split(","); */
      console.log("username", username)
      console.log("1latLongs", latLongs[keyUsername])

      const { lat, long } = latLongs[keyUsername];

      // console.log("user", user[0].center)
      const location = {
        type: 'Point',
        coordinates: [parseFloat(lat), parseFloat(long)],
      };

      if (type === 'necesidades') {
        necesidades = necesidades + 1;
      } else if (type === 'dones') {
        dones = dones + 1;
      }

      const { _categories, _community, _user } = await this.stringToId(categories, community, username);
      const users = [_user];

      const seedRow = {
        name,
        community: _community,
        categories: _categories,
        location,
        users,
        metadata: {
          dones,
          necesidades,
          descriptions: [description],
        },
        marker: '',
      };
      console.log("seedRow", seedRow)
      return seedRow;
    });

    const decorateSeed = await Promise.all(pb);
    return this.indexByName(decorateSeed);
  }

  private async stringToId(categories: Category[], community: string, username: string) {
    const pa = categories.map(async c => {
      const _c = await categoryModel.find({ name: c });
      if (_c.length !== 1) {
        throw new HttpException(404, `CATEGORY NOT FOUND!, ${c}`);
      }

      return _c[0]._id.toString();
    });

    const _categories = await Promise.all(pa);

    const _community = await communityModel.find({ name: community });
    if (_community.length !== 1) {
      throw new HttpException(404, `COMMUNITY NOT FOUND!, ${community}`);
    }

    const _user: any = await userModel.find({ name: username });
    if (_user.length !== 1) {
      throw new HttpException(404, `USER NOT FOUND!, ${username}`);
    }

    return {
      _categories,
      _community: _community[0]._id.toString(),
      _user: _user[0]._id.toString(),
    };
  }

  async processCommunities(seed: any, latLongs) {
    // Necesidad | Don
    const allCommunitiesNecesidades = seed.necesidades.map(async n => await this.getCommunitydata(n, latLongs));
    const allCommunitiesDones = seed.dones.map(async n => await this.getCommunitydata(n, latLongs));
    const allCommunitiesDuplicates = await Promise.all(allCommunitiesNecesidades.concat(allCommunitiesDones));

    let allCommunities = [...new Set(allCommunitiesDuplicates)];
    allCommunities = this.removeDuplicatesFromObj(allCommunities);
    // console.log("allCommunities", allCommunities);

    if (NODE_ENV !== 'test') {
      allCommunities.map(async (c: any) => {
        const communitiesCount = await communityModel.find({ name: c.name }).count();
        if (communitiesCount === 0) {
          await communityModel.create(c);
        }
      });
    }
    return allCommunities;
  }

  async processUsers(seed: any, latLongs) {
    // Necesidad | Don
    const allUsersNecesidades = seed.necesidades.map(async n => await this.getUserdata(n, latLongs));
    const allUsersDones = seed.dones.map(async n => await this.getUserdata(n, latLongs));
    const allUsersDuplicates = await Promise.all(allUsersNecesidades.concat(allUsersDones));

    let allUsers = [...new Set(allUsersDuplicates)];
    allUsers = this.removeDuplicatesFromObj(allUsers);
    console.log("allUsers", allUsers);

    if (NODE_ENV !== 'test') {
      const pa = allUsers.map(async (u: any) => {
        const usersCount = await userModel.find({ name: u.name }).count();
        // console.log("user", usersCount)
        if (usersCount === 0) {
          console.log('User created', u.name);
          u.username = `${u.name}${this.randomNumber()}`;
          u.email = `${this.randomNumber()}@gplaza.eus`;
          await userModel.create(u);
        }
      });
      await Promise.all(pa);
    }
    return allUsers;
  }

  private removeDuplicatesFromObj(data) {
    return Array.from(data.reduce((a, o) => a.set(o.name, o), new Map()).values());
  }

  indexByName(data) {
    return data.reduce((acc, curr) => {
      // const key = curr["name"];
      const key = `${curr['name']}-${curr['community']}`;
      const value = acc[key] ? [...acc[key], curr] : [curr];
      acc[key] = value;
      return acc;
    }, {});
  }

  private async getCommunitydata(n, latLongs) {
    const { D: name, E: address } = n;
    const keyCommunityName = name.replace(" ", "-");
    const { lat, long } = latLongs[keyCommunityName];
    // console.log("lat,long", lat, long, address)

    const community = {
      name,
      address,
      center: `${lat},${long}`,
    };

    return community;
  }

  private async getLatLong(address: string) {
    console.log("address inside", address)
    /*  const geoCoderUrl = `https://geocode.maps.co/search?q=${address}`

     let geoData = await fetch(geoCoderUrl)
     geoData = await geoData.json();
     console.log("geoData", geoData)

     const { lat, lon: long } = geoData[0];
     // console.log("lat,lon", lat, long)  */

    const geoCoderUrl = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=${address}&category=&outFields=*&forStorage=false&f=json`;

    let geoData = await fetch(geoCoderUrl)
    geoData = await geoData.json();

    /*    if (!geoData) {
         return `No geodata for ${address}`;
       }
    */
    // console.log("geoData", geoData.candidates[0].location)

    const { x: long, y: lat } = geoData.candidates[0].location;
    console.log("lat,lon", lat, long)
    return { lat, long };
  }

  private async getUserdata(n, latLongs) {
    const keyUsername = n['G'].replace(" ", "-")

    // const { lat, long } = await this.getLatLong(n['C']);
    console.log("usedata", latLongs[keyUsername])
    const { lat, long } = latLongs[keyUsername];;

    const user = {
      name: n['G'],
      contact_ways: [],
      center: `${lat},${long}`,
      // username: `${n["G"]}${this.randomNumber()}`,
      // email: `${this.randomNumber()}@gplaza.eus`
    };

    if (n['H'] !== undefined && n['I'] !== undefined) {
      user.contact_ways.push({
        type: n['H'],
        data: n['I'],
      });
    }

    if (n['J'] !== undefined && n['K'] !== undefined) {
      user.contact_ways.push({
        type: n['J'],
        data: n['K'],
      });
    }

    if (n['L'] !== undefined && n['M'] !== undefined) {
      user.contact_ways.push({
        type: n['L'],
        data: n['M'],
      });
    }

    if (n['N'] !== undefined && n['O'] !== undefined) {
      user.contact_ways.push({
        type: n['N'],
        data: n['O'],
      });
    }

    if (n['P'] !== undefined && n['Q'] !== undefined) {
      user.contact_ways.push({
        type: n['P'],
        data: n['Q'],
      });
    }

    return user;
  }

  //                  seed,          auxSeed,             type: string ??
  mixWithOpencalcData(decorateDones, decorateNecesidades, type: string) {
    const dones = [];

    Object.entries(decorateDones).map(([k, v]) => {
      if (decorateDones[k].length > 1) {
        const users = [];
        const descriptions = [];
        let _dones = 0;
        let necesidades = 0;

        // fill users, metadata.descriptions, metadata.dones, metadata.necesidades
        decorateDones[k].map((z, b) => {
          z.users.map(u => users.push(u));
          z.metadata.descriptions.map(d => descriptions.push(d));
          if (type === 'dones') {
            _dones = _dones + z.metadata.dones;
          } else if (type === 'necesidades') {
            necesidades = necesidades + z.metadata.necesidades;
          }

          let name = z.name;
          const hasSynonym = () => {
            const res = this.synonyms.find(s => name.includes(s[type]) ? true : false)
            return res != undefined ? true : false;
          }

          if (hasSynonym()) {
            name = this.toSynomyn(name, type);
          }

          if (decorateNecesidades[`${name}-${z.community}`] && b === 0) {
            const necesidadesOpencal = decorateNecesidades[`${name}-${z.community}`].length;
            if (type === 'dones') {
              necesidades = necesidades + necesidadesOpencal;
              console.log('necesidades!!!', necesidades);
            } else if (type === 'necesidades') {
              _dones = dones.length + necesidadesOpencal;
              console.log('dones!!!', _dones);
            }
          }
        });
        // console.log("users", users)

        // insert users, metadata.descriptions, metadata.dones, metadata.necesidades in decorateDones[k]
        decorateDones[k].map((z, x) => {
          // console.log("z", decorateDones[k][1])
          decorateDones[k][x].users = users;
          decorateDones[k][x].metadata.descriptions = descriptions;
          decorateDones[k][x].metadata.dones = _dones;
          decorateDones[k][x].metadata.necesidades = necesidades;
          dones.push(decorateDones[k][x]);
        });
        // console.log("decorateDones[k]", decorateDones[k])
      } else {
        const necesidades = 0;
        let name = decorateDones[k][0].name;
        const hasSynonym = () => {
          const res = this.synonyms.find(s => name.includes(s[type]) ? true : false)
          return res != undefined ? true : false;
        }

        if (hasSynonym()) {
          name = this.toSynomyn(name, type);
          console.log("invert_word", name)
        }

        if (decorateNecesidades[`${name}-${decorateDones[k][0].community}`]) {
          const necesidadesOpencal = decorateNecesidades[`${name}-${decorateDones[k][0].community}`].length;
          if (type === 'dones') {
            decorateDones[k][0].metadata.necesidades = necesidades + necesidadesOpencal;
            console.log('necesidades!!! else', necesidades, necesidadesOpencal, decorateDones[k][0].metadata.necesidades);
          } else if (type === 'necesidades') {
            decorateDones[k][0].metadata.dones = necesidades + necesidadesOpencal;
            console.log('dones!!! else', dones, necesidadesOpencal, decorateDones[k][0].metadata.dones);
          }
        }
        // console.log("RESULTADO!", decorateDones[k][0])
        dones.push(decorateDones[k][0]);
      }
    });

    return dones;
  }

  mixWithDBData(dones, type: string) {
    // console.log("--- DONES", dones)
    return dones.map(async (d, x) => {
      // console.log("d", d)
      let name = d.name;
      let nameForDon = d.name;
      let nameForNecesidad = d.name;

      const hasSynonym = () => {
        const res = this.synonyms.find(s => name.includes(s[type]) ? true : false)
        return res != undefined ? true : false;
      }
      if (hasSynonym()) {
        nameForNecesidad = this.toSynomyn(name, "dones");
        nameForDon = this.toSynomyn(name, "necesidades");
      }

      const isNewNecesidad = await necesidadModel.find({ name: nameForNecesidad, community: d.community }).count();
      const isNecesidad = await necesidadModel.find({ name: nameForNecesidad, community: d.community }).populate('metadata');
      // console.log("isNewNecesidad", isNewNecesidad, isNecesidad, nameForNecesidad)
      dones[x].metadata.necesidades = dones[x].metadata.necesidades + isNewNecesidad;

      const allDOns = await donModel.find();
      // console.log("allDOns", allDOns)
      const isNewDon = await donModel.find({ name: nameForDon, community: d.community }).count();
      const isDon = await donModel.find({ name: nameForDon, community: d.community }).populate('metadata');
      // console.log("isNewDon", isNewDon, isDon, nameForDon)
      if (type === 'dones') {
        dones[x].metadata.dones = dones[x].metadata.dones + isNewDon;
      } else if (type === 'necesidades') {
        dones[x].metadata.dones = dones[x].metadata.dones + isNewDon;
      }

      /* if (isDon.length > 0 && !dones[x].users.includes(isDon[0].users[0].toString())) {
                dones[x].users.push(isDon[0].users[0].toString());
            } */

      if (type === 'dones') {
        dones[x].__isNewMetadata = true;
        // dones[x].__sameIds = [];

        if (isDon.length > 0) {
          dones[x].__isNewMetadata = isDon[0].metadata._id.toString();
          // console.log('IS DON', isDon, dones[x]);
          isDon.map((d, z) => {
            d.users.map((s, h) => {
              if (!dones[x].users.includes(isDon[z].users[h].toString())) {
                dones[x].users.push(isDon[z].users[h].toString());
              }
            });

            d.metadata.descriptions.map((s, h) => {
              if (!dones[x].metadata.descriptions.includes(isDon[z].metadata.descriptions[h].toString())) {
                dones[x].metadata.descriptions.push(isDon[z].metadata.descriptions[h].toString());
              }
            });
          });
        }
      } else if (type === 'necesidades') {
        dones[x].__isNewMetadata = true;
        // dones[x].__sameIds = [];

        if (isNecesidad.length > 0) {
          dones[x].__isNewMetadata = isNecesidad[0].metadata._id.toString();

          isNecesidad.map((d, z) => {
            d.users.map((s, h) => {
              if (!dones[x].users.includes(isNecesidad[z].users[h].toString())) {
                dones[x].users.push(isNecesidad[z].users[h].toString());
              }
            });

            d.metadata.descriptions.map((s, h) => {
              if (!dones[x].metadata.descriptions.includes(isNecesidad[z].metadata.descriptions[h].toString())) {
                dones[x].metadata.descriptions.push(isNecesidad[z].metadata.descriptions[h].toString());
              }
            });
          });
        }
      }
      // console.log("FINAL!", d)
      return d;

      /* if (isDon.length > 0 && !dones[x].metadata.descriptions.includes(isDon[0].metadata.descriptions[0].toString())) {
                dones[x].metadata.descriptions.push(isDon[0].metadata.descriptions[0].toString());
            } */

      /* if (isDon.length > 0) {
                isDon.map((d, z) => {
                    d.metadata.descriptions.map((s, h) => {
                        if (!dones[x].metadata.descriptions.includes(isDon[z].metadata.descriptions[h].toString())) {
                            dones[x].metadata.descriptions.push(isDon[z].metadata.descriptions[h].toString());
                        }
                    })
                })
            } */
    });
  }

  async processMetadata(dones, type: string) {
    dones = this.indexByName(dones);
    // console.log("INICIO", dones)

    const _dones = [];
    const a = Object.entries(dones);
    // console.log("entra entra")
    const px = a.map(async ([k, v]) => {
      // console.log("dones[k]", dones[k])
      if (dones[k][0].__isNewMetadata === true) {
        const { metadata } = dones[k][0];
        let metadata_id = null;
        // console.log('entra entra', metadata);
        if (type === "dones") {
          metadata_id = await donMetadataModel.create(metadata);
        } else if (type === "necesidades") {
          metadata_id = await necesidadMetadataModel.create(metadata);
        }
        dones[k].map(d => {
          d.metadata = metadata_id._id.toString();
          delete d.__isNewMetadata;
          _dones.push(d);
        });
        // console.log('fin crear metadata!!!');
      } else {
        const id = dones[k][0].__isNewMetadata;
        console.log('existe. usar ese don_id que viene como dato y su metadata_id', id);
        const pc = dones[k].map(async d => {
          // console.log('next', d.metadata);
          let name = d.name;
          const hasSynonym = () => {
            const res = this.synonyms.find(s => name.includes(s[type]) ? true : false)
            return res != undefined ? true : false;
          }

          if (hasSynonym()) {
            name = this.toSynomyn(name, type === "dones" ? "necesidades" : "dones");
          }
          if (type === "dones") {
            await donMetadataModel.updateOne({ _id: id }, d.metadata);
          } else if (type === "necesidades") {
            await necesidadMetadataModel.updateOne({ _id: id }, d.metadata);
          }
          const isDon = await donModel.find({ name, community: d.community });
          // const allNecesidades = await necesidadModel.find();
          // const isNecesidad = await necesidadModel.find({ name: d.name, community: d.community });
          // const SDF = isDon.map(d => d.users.map(u => u.toString())[0]);
          // console.log("isDon", isDon, d.users, SDF)
          if (isDon.length > 0) {
            const users = Array.from(new Set(d.users.concat(isDon.map(d => d.users.map(u => u.toString())[0]))));
            // console.log('users', users);
            const pf = isDon.map(async m => {
              const done = await donModel.updateOne({ _id: m._id.toString() }, { users });
              // console.log('done', done);
            });
            await Promise.all(pf);
          }
          // console.log('updated', asd);
          d.metadata = id;
          _dones.push(d);
        });
        await Promise.all(pc);
      }
    });
    await Promise.all(px);
    // console.log("FIN", _dones)
    return _dones;
  }

  async processDones(dones): Promise<void> {
    const py = dones.map(async d => {
      await donModel.create(d);
      console.log('Don has been created');
    });

    await Promise.all(py);
  }

  async processNecesidades(necesidades): Promise<void> {
    const py = necesidades.map(async n => {
      await necesidadModel.create(n);
      console.log('Necesidad has been created');
    });
    await Promise.all(py);

    // update on necesidades. could add the same seed name and community after adds user on mixWithDBData
    const pg = necesidades.map(async d => {
      const isNecesidad = await necesidadModel.find({ name: d.name, community: d.community });
      const allNecesidades = await necesidadModel.find();
      console.log('isNecesidad', isNecesidad, allNecesidades);
      if (isNecesidad.length > 0) {
        const users = Array.from(new Set(d.users.concat(isNecesidad.map(d => d.users.map(u => u.toString())[0]))));
        const pl = isNecesidad.map(async n => {
          const done = await necesidadModel.updateOne({ _id: n._id.toString() }, { users });
          // console.log('done2', done);
        });
        await Promise.all(pl);
      }
    });

    await Promise.all(pg);
  }

  /* private firstUppercase(name: string) {
        return name.charAt(0).toUpperCase() + name.slice(1);
    } */

  private randomNumber() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  private toSynomyn(name: any, type: string) {
    let rest = name.split(" ");
    rest = rest.slice(1, rest.length);
    rest = rest.join(" ");

    let key = this.synonyms.findIndex(s => name.includes(s[type]));
    if (key <= -1) key = 0;

    let invert_word = type !== "necesidades" ? this.synonyms[key]["necesidades"] : this.synonyms[key]["dones"];
    invert_word = `${invert_word} ${rest}`;

    return invert_word;
  }

  async fillGetLang(jsonData) {
    const addresses = [];
    jsonData.dones.map(data => {
      let { C: name, D: communityName, E: community, G: username } = data;
      username = username.replace(" ", "-")
      communityName = communityName.replace(" ", "-");
      if (!addresses[communityName]) {
        addresses[communityName] = community;
      }
      if (!addresses[username]) {
        addresses[username] = name;
      }

      return data;
    })

    jsonData.necesidades.map(data => {
      let { C: name, D: communityName, E: community, G: username } = data;
      username = username.replace(" ", "-")
      communityName = communityName.replace(" ", "-");

      if (!addresses[communityName]) {
        addresses[communityName] = community;
      }
      console.log("n", data)
      if (!addresses[username]) {
        addresses[username] = name;
      }
    })

    console.log("ADRESS", addresses)
    for (let address in addresses) {
      console.log("aqui", addresses[address])
      addresses[address] = await this.getLatLong(addresses[address])
    }

    return addresses;
  }

  addLatLongOrigData(jsonData, latLongs) {
    const jsonNecesidades = jsonData.necesidades.map(n => {
      n["C"] = latLongs[n["D"]]
      n["E"] = latLongs[n["G"]]

      return n;
    })
    const jsonDones = jsonData.dones.map(d => {
      d["C"] = latLongs[d["D"]]
      d["E"] = latLongs[d["G"]]

      return d;
    })

    return { jsonNecesidades, jsonDones }
  }
}

export class MigratorService {
  dones: Don[];
  // necesidades: Necesidad[];
  communities: Community[];

  constructor() {
    // this.connect();
  }

  async connect() {
    const c = await connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_DATABASE}`);
    // c.model("user").find({})

  }

  async allCommunities() {
    this.communities = await communityModel.find();
  }

  async allDones() {
    this.dones = await donModel.find();
  }

  /*     async allNecesidades() {
            this.necesidades = await necesidadModel.find();
        } */

  async disconnect() {
    await disconnect();
  }
}

// TODO: crear interface. Usar JsonDb implements Database que tiene source y readDoc()
export class Database {
  source: string;

  constructor(source: string) {
    this.source = source;
  }

  readDoc() {
    return excelToJson({
      sourceFile: this.source,
    });
  }
}

async function main() {
  let latLongs = [];
  const odtSource = './data/migrationPrueba2.ods';
  // const odtSource = './data/migrationPruebaSinonimos.ods';
  const migrator = new Migrator();
  const migratorService = new MigratorService();
  await migratorService.connect();

  // step0. get data from odt to json without odt headers
  const jsonData: any = new Database(odtSource).readDoc();

  jsonData.necesidades.shift();
  jsonData.dones.shift();


  latLongs = await migrator.fillGetLang(jsonData);
  console.log("latLongs", latLongs)
  const { jsonNecesidades, jsonDones } = await migrator.addLatLongOrigData(jsonData, latLongs);

  // -step1. get and save new communities and users
  console.log("jsonData", jsonData)
  await migrator.processCommunities(jsonData, latLongs);
  await migrator.processUsers(jsonData, latLongs);

  // step2. Start with dones and necesidades
  // step2-1. Decorate and index dones by name
  const decorateDones = await migrator.decorateObject(jsonDones, 'dones', latLongs);
  console.log("decorateDones", decorateDones);

  // step2-2. Decorate and index necesidades by name
  const decorateNecesidades = await migrator.decorateObject(jsonNecesidades, 'necesidades', latLongs);
  console.log("decorateNecesidades", decorateNecesidades);

  // step3. Mix dones with open calc and database. then save to dons collections
  let dones = migrator.mixWithOpencalcData(decorateDones, decorateNecesidades, 'dones');
  const pa = migrator.mixWithDBData(dones, 'dones');
  await Promise.all(pa);

  dones = await migrator.processMetadata(dones, 'dones');
  await migrator.processDones(dones);

  // console.log("process necesidades!!!", decorateDones)

  // step4. Mix necesidades with open calc and database. then save to necesidads collections
  let necesidades = migrator.mixWithOpencalcData(decorateNecesidades, decorateDones, 'necesidades');
  const pb = migrator.mixWithDBData(necesidades, 'necesidades');
  await Promise.all(pb);

  // console.log("necesidades", necesidades, necesidades[1].metadata)
  necesidades = await migrator.processMetadata(necesidades, 'necesidades');
  console.log("necesidades", necesidades)
  await migrator.processNecesidades(necesidades);

  /*  const odtSource2 = './data/migrationPrueba2.ods';


   // step0. get data from odt to json without odt headers
   const jsonData2: any = new Database(odtSource2).readDoc();

   jsonData2.necesidades.shift();
   jsonData2.dones.shift();

   const jsonNecesidades2 = jsonData2.necesidades;
   const jsonDones2 = jsonData2.dones;
   // console.log("jsonData", jsonNecesidades);

   // step1. get and save new communities and users
   // console.log("jsonData", jsonData)
   await migrator.processCommunities(jsonData2);
   await migrator.processUsers(jsonData2);

   // step2. Start with dones and necesidades
   // step2-1. Decorate and index dones by name
   const decorateDones2 = await migrator.decorateObject(jsonDones2, 'dones');
   // console.log("decorateDones", decorateDones);

   // step2-2. Decorate and index necesidades by name
   const decorateNecesidades2 = await migrator.decorateObject(jsonNecesidades2, 'necesidades');
   // console.log("decorateNecesidades", decorateNecesidades);

   // const necesidades = [];

   // step3. Mix dones with open calc and database. then save to dons collections
   let dones2 = migrator.mixWithOpencalcData(decorateDones2, decorateNecesidades2, 'dones');
   const pa2 = migrator.mixWithDBData(dones2, 'dones');
   await Promise.all(pa2);
   console.log('dones2', dones);
   dones2 = await migrator.processMetadata(dones2);
   await migrator.processDones(dones2);

   // console.log("process necesidades2222!!!", decorateDones2)

   // step4. Mix necesidades with open calc and database. then save to necesidads collections
   let necesidades2 = migrator.mixWithOpencalcData(decorateNecesidades2, decorateDones2, 'necesidades');
   const pb2 = migrator.mixWithDBData(necesidades2, 'necesidades');
   await Promise.all(pb2);

   // console.log("necesidades", necesidades2, necesidades2[1].metadata)
   necesidades2 = await migrator.processMetadata(necesidades2);
   await migrator.processNecesidades(necesidades2); */

  /*  await migratorService.allCommunities();
     console.log("end", migratorService.communities); */
}

if (NODE_ENV !== 'test') {
  main();
}
