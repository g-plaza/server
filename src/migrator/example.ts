// https://geocode.maps.co/search?q=Alameda%20del%20Boulevard,%2018,%2020003%20San%20Sebasti%C3%A1n%20San%20Sebasti%C3%A1n%20Espa%C3%B1a -> 43.3418968	-1.8073704

import { add } from "winston";

// https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=Alameda%20del%20Boulevard,%2018,%2020003%20San%20Sebasti%C3%A1n%20San%20Sebasti%C3%A1n%20Espa%C3%B1a&category=&outFields=*&forStorage=false&f=json

// npm run build && node dist/migrator/example.js
// https://gps-coordinates.org/coordinate-converter.php
const fetch = require("node-fetch")

async function main() {
  // lat,lon 43.3224632 -1.9846375
  // const address = "Alameda del Boulevard, 18, 20003 San Sebastián España";
  // const address = "Alameda del Boulevard, 18, 20003 San Sebastián";
  // const address = "Alameda del Boulevard, 18, San Sebastián";

  // lat,lon 43.315568 -2.0063915
  // const address = "Igeldo Pasealekua, 20008 Donostia";
  // const address = "Igeldo Pasealekua, 20008 San Sebastián España";
  // const address = "Igeldo Pasealekua, 20008 San Sebastián";
  // const address = "Igeldo Pasealekua, 20008 San Sebastian";

  const address = "Calle San Francisco Javier, 11, 20303 Irún";
  // const geoCoderUrl = `https://geocode.maps.co/search?q=${address}`
  const geoCoderUrl = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=${address}&category=&outFields=*&forStorage=false&f=json`;

  let geoData = await fetch(geoCoderUrl)
  geoData = await geoData.json();
  console.log("geoData", geoData.candidates[0].location)

  // const { lat, lon } = geoData[0];
  const { x: lat, y: long } = geoData.candidates[0].location
  console.log("lat,lon", lat, long)
}
main()


