/*
 * IMPORTANTE!
 *
 * Para que pasen los tests necesitan unos datos de prueba
 * para categorias, usuarios y comunidades.
 * Esos datos estan en /src/tests/data/dataNeedForTest/*.json
 *
 * npm run test:migrator
 *
*/

import { connect } from 'mongoose';
import { Migrator } from '../migrator';
import { testData, testLatLongs } from './data/testData';
import communityModel from '../models/community.model';
import categoryModel from '../models/category.model';
import usersModel from '../models/users.model';
import donModel from '../models/don.model';
import necesidadModel from '../models/necesidad.model';
import donMetadataModel from '@models/dons-metadata.model'
import necesidadMetadataModel from '@models/necesidads-metadata.model'


describe('Migrator', () => {
  const migrator = new Migrator();

  afterAll(async () => {
    console.log("Deleting data if exists on dons and necesidads collections.")
    await donModel.deleteMany({});
    await necesidadModel.deleteMany({});
  })

  describe('[fn] processCommunities(data)', () => {
    it('1 don / 1 necesidad => 2 comunities', async () => {
      const { dones, necesidades } = testData.test1;

      const res = await migrator.processCommunities({ dones, necesidades });
      const [lat0, long0] = res[0].center.split(",");
      const [lat1, long1] = res[1].center.split(",");

      const like = [
        { name: necesidades[0]['D'], address: necesidades[0]['E'], center: `${lat0},${long0}` },
        { name: dones[0]['D'], address: dones[0]['E'], center: `${lat1},${long1}` },
      ];
      console.log("like", like)
      expect(res).toEqual(like);
    });

    it('2 dones / 0 necesidades => 1 comunity', async () => {
      const { dones, necesidades } = testData.test2;

      const res = await migrator.processCommunities({ dones, necesidades });
      const [lat, long] = res[0].center.split(",");

      const like = [{ name: dones[0]['D'], address: dones[0]['E'], center: `${lat},${long}` }];

      expect(res).toEqual(like);
    });

    it('0 dones / 2 necesidades => 1 comunity', async () => {
      const { dones, necesidades } = testData.test3;

      const res = await migrator.processCommunities({ dones, necesidades });
      const [lat, long] = res[0].center.split(",");

      const like = [{ name: necesidades[0]['D'], address: necesidades[0]['E'], center: `${lat},${long}` }];

      expect(res).toEqual(like);
    });

    it('1 dones / 2 necesidades => 2 comunities', async () => {
      const { dones, necesidades } = testData.test4;

      const res = await migrator.processCommunities({ dones, necesidades });
      const [lat0, long0] = res[0].center.split(",");
      const [lat1, long1] = res[1].center.split(",");

      const like = [
        { name: necesidades[0]['D'], address: necesidades[0]['E'], center: `${lat0},${long0}` },
        { name: necesidades[1]['D'], address: necesidades[1]['E'], center: `${lat1},${long1}` },
      ];
      expect(res).toEqual(like);
    });

    it('1 dones / 2 necesidades => 3 comunities', async () => {
      const { dones, necesidades } = testData.test5;

      const res = await migrator.processCommunities({ dones, necesidades });

      const [lat0, long0] = res[0].center.split(",");
      const [lat1, long1] = res[1].center.split(",");
      const [lat2, long2] = res[2].center.split(",");

      const like = [
        { name: necesidades[0]['D'], address: necesidades[0]['E'], center: `${lat0},${long0}` },
        { name: necesidades[1]['D'], address: necesidades[1]['E'], center: `${lat1},${long1}` },
        { name: dones[0]['D'], address: dones[0]['E'], center: `${lat2},${long2}` },
      ];

      expect(res).toEqual(like);
    });

    it('0 dones / 0 necesidades => 0 comunities', async () => {
      const { dones, necesidades } = testData.test6;
      const like = [];

      const res = await migrator.processCommunities({ dones, necesidades });
      expect(res).toEqual(like);
    });
  });

  describe('[fn] processUsers(data)', () => {
    it('2 users => 1 unique user', async () => {
      const { dones, necesidades } = testData.test1;

      const res: any = await migrator.processUsers({ dones, necesidades });
      console.log("RES 00000", res)
      const [lat, long] = res[0].center.split(",");

      const like = [
        {
          name: necesidades[0]['G'],
          contact_ways: [{ type: necesidades[0]['H'], data: necesidades[0]['I'] }],
          center: `${lat},${long}`,
        },
      ];

      expect(res).toEqual(like);
    });

    it('2 users => 2 unique users', async () => {
      const { dones, necesidades } = testData.test3;

      const res: any = await migrator.processUsers({ dones, necesidades });
      const [lat0, long0] = res[0].center.split(",");
      const [lat1, long1] = res[1].center.split(",");

      const like = [
        {
          name: necesidades[0]['G'],
          contact_ways: [
            { type: necesidades[0]['H'], data: necesidades[0]['I'] },
            { type: necesidades[0]['J'], data: necesidades[0]['K'] },
            { type: necesidades[0]['L'], data: necesidades[0]['M'] },
            { type: necesidades[0]['N'], data: necesidades[0]['O'] },
          ],
          center: `${lat0},${long0}`,
        },
        {
          name: necesidades[1]['G'],
          contact_ways: [
            { type: necesidades[1]['H'], data: necesidades[1]['I'] },
            { type: necesidades[1]['J'], data: necesidades[1]['K'] },
            { type: necesidades[1]['L'], data: necesidades[1]['M'] },
            { type: necesidades[1]['N'], data: necesidades[1]['O'] },
            { type: necesidades[1]['P'], data: necesidades[1]['Q'] },
          ],
          center: `${lat1},${long1}`,
        },
      ];

      expect(res).toEqual(like);
    });

    it('3 users => 2 unique users', async () => {
      const { dones, necesidades } = testData.test4;

      const res: any = await migrator.processUsers({ dones, necesidades });
      const [lat0, long0] = res[0].center.split(",");
      const [lat1, long1] = res[1].center.split(",");
      const like = [
        {
          name: necesidades[0]['G'],
          contact_ways: [{ type: necesidades[0]['H'], data: necesidades[0]['I'] }],
          center: `${lat0},${long0}`,
        },
        {
          name: necesidades[1]['G'],
          contact_ways: [{ type: necesidades[1]['H'], data: necesidades[1]['I'] }],
          center: `${lat1},${long1}`,
        },
      ];

      expect(res).toEqual(like);
    });

    it('contact ways - One contact way: telegram', async () => {
      const { dones, necesidades } = testData.test1;
      const like = [
        { type: necesidades[0]['H'], data: necesidades[0]['I'] }
      ];

      const res: any = await migrator.processUsers({ dones, necesidades });
      expect(res[0].contact_ways).toEqual(like);
    });

    it('contact ways - Two contact ways: telegram + email', async () => {
      const { dones, necesidades } = testData.test2;
      const like = [
        { type: dones[0]['H'], data: dones[0]['I'] },
        { type: dones[0]['J'], data: dones[0]['K'] },
      ];

      const res: any = await migrator.processUsers({ dones, necesidades });
      expect(res[0].contact_ways).toEqual(like);
    });

    it('contact ways - Three contact ways: telegram + email + telefono', async () => {
      const { dones, necesidades } = testData.test2;
      const like = [
        { type: dones[1]['H'], data: dones[1]['I'] },
        { type: dones[1]['J'], data: dones[1]['K'] },
        { type: dones[1]['L'], data: dones[1]['M'] },
      ];

      const res: any = await migrator.processUsers({ dones, necesidades });
      expect(res[1].contact_ways).toEqual(like);
    });

    it('contact ways - Four contact ways: telegram + email + telefono + web', async () => {
      const { dones, necesidades } = testData.test3;
      const like = [
        { type: necesidades[0]['H'], data: necesidades[0]['I'] },
        { type: necesidades[0]['J'], data: necesidades[0]['K'] },
        { type: necesidades[0]['L'], data: necesidades[0]['M'] },
        { type: necesidades[0]['N'], data: necesidades[0]['O'] },
      ];

      const res: any = await migrator.processUsers({ dones, necesidades });
      expect(res[0].contact_ways).toEqual(like);
    });

    it('contact ways - Five contact ways: telegram + email + telefono + web + another one', async () => {
      const { dones, necesidades } = testData.test3;
      const like = [
        { type: necesidades[1]['H'], data: necesidades[1]['I'] },
        { type: necesidades[1]['J'], data: necesidades[1]['K'] },
        { type: necesidades[1]['L'], data: necesidades[1]['M'] },
        { type: necesidades[1]['N'], data: necesidades[1]['O'] },
        { type: necesidades[1]['P'], data: necesidades[1]['Q'] },
      ];

      const res: any = await migrator.processUsers({ dones, necesidades });
      expect(res[1].contact_ways).toEqual(like);
    });
  });

  describe('[fn] decorateObject(seed, type)', () => {
    it('decorate dones', async () => {
      const { dones } = testData.test1;
      const [lat, long] = testLatLongs[dones[0]['G']];
      const { communityId, categories, users } = await _stringToId(dones);

      const like = {
        [`${dones[0]['A']}-${communityId}`]: [
          {
            name: dones[0]['A'],
            categories: categories,
            community: communityId,
            location: { type: 'Point', coordinates: [lat, long] },
            users: users,
            metadata: { dones: 1, necesidades: 0, descriptions: [dones[0]['F']] },
            marker: '',
          },
        ],
      };

      const res = await migrator.decorateObject(dones, 'dones');
      expect(res).toEqual(like);
    });

    it('decorate necesidades', async () => {
      const { necesidades } = testData.test1;
      const [lat, long] = testLatLongs[necesidades[0]['G']];
      const { communityId, categories, users } = await _stringToId(necesidades);

      const like = {
        [`${necesidades[0]['A']}-${communityId}`]: [
          {
            name: necesidades[0]['A'],
            categories: categories,
            community: communityId,
            location: { type: 'Point', coordinates: [lat, long] },
            users: users,
            metadata: { dones: 0, necesidades: 1, descriptions: [necesidades[0]['F']] },
            marker: '',
          },
        ],
      };

      const res = await migrator.decorateObject(necesidades, 'necesidades');
      expect(res).toEqual(like);
    });
    async function _stringToId(dones) {
      const categories: any = [];
      // const users: any = [];

      const { DB_USER, DB_PASS, DB_HOST, DB_DATABASE } = process.env;
      await connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_DATABASE}`);

      // get community_id
      let communityId: any = await communityModel.find({ name: dones[0]['D'] });
      // console.log("communityId", communityId);
      communityId = communityId[0]._id.toString();

      // get categories_id
      dones[0]['B'].split(',').map(async cat => {
        const categoryId = await categoryModel.find({ name: cat });
        categories.push(categoryId[0]._id.toString());
      });

      // get userId
      let userId: any = await usersModel.find({ name: dones[0]['G'] });
      userId = userId[0]._id.toString();

      return { communityId, categories, users: [userId] };
    }
  });

  describe('[fn] mixWithOpencalcData(decorateDones, decorateNecesidades)', () => {
    // Take in account. Necesidads are add only for counting in metadata.necesidades.
    // Happens on all [mixed dones] tests.
    it('mixed dones. 1 don + 1 necesidad => same community, different seed name', async () => {
      let { dones, necesidades } = testData.test7;

      const like = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: dones[0].users,
          metadata: { dones: 1, necesidades: 0, descriptions: dones[0].metadata.descriptions },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);

      const res = await migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      expect(res).toEqual(like);
    });

    it('mixed dones. 1 don + 1 necesidad => same community, same seed name', async () => {
      let { dones, necesidades } = testData.test8;
      const like = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: dones[0].users,
          metadata: { dones: 1, necesidades: 1, descriptions: dones[0].metadata.descriptions },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);

      const res = await migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      expect(res).toEqual(like);
    });

    it('mixed dones. 3 don + 1 necesidad => different communities, 3 with same seed name + 1 different', async () => {
      let { dones, necesidades } = testData.test9;
      const like = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 0, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: dones[1].name,
          community: dones[1].community,
          categories: dones[1].categories,
          location: { type: 'Point', coordinates: dones[1].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 0, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: dones[2].name,
          community: dones[2].community,
          categories: dones[2].categories,
          location: { type: 'Point', coordinates: dones[2].location.coordinates },
          users: [dones[2].users[0]],
          metadata: { dones: 1, necesidades: 0, descriptions: [dones[2].metadata.descriptions[0]] },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);

      const res = await migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      expect(res).toEqual(like);
    });

    it('mixed dones. 3 don + 1 necesidad => same community except 1 don, same seed name', async () => {
      let { dones, necesidades } = testData.test10;
      const like = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 1, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: dones[1].name,
          community: dones[1].community,
          categories: dones[1].categories,
          location: { type: 'Point', coordinates: dones[1].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 1, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: dones[2].name,
          community: dones[2].community,
          categories: dones[2].categories,
          location: { type: 'Point', coordinates: dones[2].location.coordinates },
          users: [dones[2].users[0]],
          metadata: { dones: 1, necesidades: 0, descriptions: [dones[2].metadata.descriptions[0]] },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);

      const res = await migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      expect(res).toEqual(like);
    });

    it('mixed dones. 2 don + 1 necesidad => same community, same seed name', async () => {
      let { dones, necesidades } = testData.test11;
      const like = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 1, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: dones[1].name,
          community: dones[1].community,
          categories: dones[1].categories,
          location: { type: 'Point', coordinates: dones[1].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 1, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);

      const res = await migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      expect(res).toEqual(like);
    });
  });

  describe('[fn] mixWithDBData(dones)', () => {
    // when  the test starts all the time the mongo test bd is empty
    it('mixed dones. empty dones + necesidades collections. 2 don + 1 necesidad => same community, same seed name ', async () => {
      const { dones } = testData.test12;
      const like = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: dones[0].users,
          metadata: { dones: 2, necesidades: 1, descriptions: dones[0].metadata.descriptions },
          marker: '',
          __isNewMetadata: true,
        },
        {
          name: dones[1].name,
          community: dones[1].community,
          categories: dones[1].categories,
          location: { type: 'Point', coordinates: dones[1].location.coordinates },
          users: dones[1].users,
          metadata: { dones: 2, necesidades: 1, descriptions: dones[1].metadata.descriptions },
          marker: '',
          __isNewMetadata: true,
        },
      ];
      const pa = migrator.mixWithDBData(dones, 'dones');
      const res = await Promise.all(pa);
      expect(res).toEqual(like);
    });

    // TODO:
    // - (pre) add one don and necesidad for Donosti?/one community for "limpiar comunidad de vecinos"
    // using test12, get dones +1 and necesidades +1, users, descriptions
    // - (post) delete one don and necesidad

    // -(pre) add one don and necesidad for Irun?/one community (not donosti) for "limpiar comunidad de vecinos"
    // using test12, get another don and necesidad, users, descriptions
    // - (post) delete one don and necesidad

    // -(pre) add one don and necesidad for Donosti?/one community for "aprender inglés"
    // using test12, get another don and necesidad, users, descriptions
    // - (post) delete one don and necesidad

    // -(pre) add one don and necesidad for Irun?/one community (not donosti) for "aprender inglés"
    // using test12, get another don and necesidad, users, descriptions
    // - (post) delete one don and necesidad
  });

  describe('[fn] processMetadata(dones)', () => {
    it('three new dones, create metadata', async () => {
      // before
      const { dones } = testData.test13;

      // process
      const res = await migrator.processMetadata(dones, "dones");

      const metadataRes = Array.from(new Set(res.map((r: any) => r.metadata.toString())));
      const ph = metadataRes.map(async mr => {
        const metadata = await donMetadataModel.find({ _id: mr });
        expect(metadata.length).toEqual(1);
      })
      await Promise.all(ph);

      // after
      const pg = metadataRes.map(async m => {
        await donMetadataModel.deleteOne({ _id: m.toString() });
      })
      await Promise.all(pg);
    });

    it('one seed on bd, updates their dones with new users', async () => {
      // before
      const { dones } = testData.test14;

      const donMetadata = {
        necesidades: 1,
        dones: 1,
        descriptions: ["description1"]
      }
      const { _id: donMetadataId } = await donMetadataModel.create(donMetadata);

      const don = {
        name: 'limpiar comunidad de vecinos',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: { type: 'Point', coordinates: ['43.3172119368173', '-1.97544130496173'] },
        users: ['6346055bf1d3ca76e73f01b9'],
        metadata: donMetadataId,
        marker: '',
      };
      const { _id: donId } = await donModel.create(don);

      // process
      const res = await migrator.processMetadata(dones, "dones");

      const donAfter: any = await donModel.find({ name: don.name, community: don.community });
      donAfter.map(da => expect(da.users.length).toEqual(2));

      // after
      await donModel.deleteOne({ _id: donId.toString() });
      await donMetadataModel.deleteOne({ _id: donMetadataId.toString() });
    });
  });

  // processDones(dones) create new dones on mongo document, not need to test it.

  describe('[fn] processNecesidades(necesidades)', () => {
    it('three new necesidades, create metadata', async () => {
      const { necesidades } = testData.test16;

      await migrator.processNecesidades(necesidades);

      console.log("necesidades", necesidades);

      const pa = necesidades.map(async n => {
        const necesidadAfter: any = await necesidadModel.find({ name: n.name, community: n.community });
        necesidadAfter.map(na => expect(na.users.length).toEqual(1));
      })
      await Promise.all(pa);
    })

    it('one necesidad on bd, updates their necesidades with new users', async () => {
      // before
      const { necesidades } = testData.test15;

      const metadata = await donMetadataModel.find();
      const necesidad = {
        name: 'pasear perros',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['634c1976d1c1dead2de0a7dd'],
        location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
        users: ['6344766175c0ac1078cbc512'],
        metadata: metadata[0]._id,
        marker: ''
      }
      const { _id: necesidadId } = await necesidadModel.create(necesidad);

      // process
      const res = await migrator.processNecesidades(necesidades);

      const necesidadAfter: any = await necesidadModel.find({ name: necesidad.name, community: necesidad.community });
      necesidadAfter.map(na => expect(na.users.length).toEqual(2));

      // after
      await necesidadModel.deleteOne({ _id: necesidadId.toString() });
    })
  })

  describe('[process] synomys with dictionary - [fn] mixWithOpencalcData', () => {
    it('one synomyn don. one synomyn necesidad. dones:1/necesidades:1 on each', async () => {
      let { dones, necesidades } = testData.test17;

      const likeDones = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: dones[0].users,
          metadata: { dones: 1, necesidades: 1, descriptions: dones[0].metadata.descriptions },
          marker: '',
        },
      ];

      const likeNecesidades = [
        {
          name: necesidades[0].name,
          community: necesidades[0].community,
          categories: necesidades[0].categories,
          location: { type: 'Point', coordinates: necesidades[0].location.coordinates },
          users: necesidades[0].users,
          metadata: { dones: 1, necesidades: 1, descriptions: necesidades[0].metadata.descriptions },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);

      const resDones = migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      const resNecesidades = migrator.mixWithOpencalcData(necesidades, dones, 'necesidades');

      expect(resDones).toEqual(likeDones);
      expect(resNecesidades).toEqual(likeNecesidades);
    })

    it('two synomyn dones. two synomyn necesidades. dones:2/necesidades:2 on each', async () => {
      let { dones, necesidades } = testData.test18;

      const likeDones = [
        {
          name: dones[0].name,
          community: dones[0].community,
          categories: dones[0].categories,
          location: { type: 'Point', coordinates: dones[0].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 2, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: dones[1].name,
          community: dones[1].community,
          categories: dones[1].categories,
          location: { type: 'Point', coordinates: dones[1].location.coordinates },
          users: [dones[0].users[0], dones[1].users[0]],
          metadata: { dones: 2, necesidades: 2, descriptions: [dones[0].metadata.descriptions[0], dones[1].metadata.descriptions[0]] },
          marker: '',
        },
      ];

      const likeNecesidades = [
        {
          name: necesidades[0].name,
          community: necesidades[0].community,
          categories: necesidades[0].categories,
          location: { type: 'Point', coordinates: necesidades[0].location.coordinates },
          users: [necesidades[0].users[0], necesidades[1].users[0]],
          metadata: { dones: 2, necesidades: 2, descriptions: [necesidades[0].metadata.descriptions[0], necesidades[1].metadata.descriptions[0]] },
          marker: '',
        },
        {
          name: necesidades[1].name,
          community: necesidades[1].community,
          categories: necesidades[1].categories,
          location: { type: 'Point', coordinates: necesidades[1].location.coordinates },
          users: [necesidades[0].users[0], necesidades[1].users[0]],
          metadata: { dones: 2, necesidades: 2, descriptions: [necesidades[0].metadata.descriptions[0], necesidades[1].metadata.descriptions[0]] },
          marker: '',
        },
      ];

      dones = await migrator.indexByName(dones);
      necesidades = await migrator.indexByName(necesidades);
      const resDones = migrator.mixWithOpencalcData(dones, necesidades, 'dones');
      const resNecesidades = migrator.mixWithOpencalcData(necesidades, dones, 'necesidades');

      expect(resDones).toEqual(likeDones);
      expect(resNecesidades).toEqual(likeNecesidades);
    })

    describe('[process] synomys with dictionary - [fn] mixWithDBData', () => {
      it('one synomyn don + one synomyn necesidad on bd, updates their necesidades with new users, dones and necesidades', async () => {
        // before
        let { dones, necesidades } = testData.test17;

        const donMetadata = {
          necesidades: 1,
          dones: 1,
          descriptions: ["enseñaringlesDB"]
        }
        const necesidadMetadata = {
          necesidades: 1,
          dones: 1,
          descriptions: ["aprenderinglesDB"]
        }

        const { _id: donMetadataId } = await donMetadataModel.create(donMetadata);
        const { _id: necesidadMetadataId } = await necesidadMetadataModel.create(necesidadMetadata);

        const don = {
          name: 'enseñar inglés',
          community: '6346055bf1d3ca76e73f01b5',
          categories: ['634c1976d1c1dead2de0a7dd'],
          location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
          users: ['6344766175c0ac1078cbc512'],
          metadata: donMetadataId,
          marker: ''
        }
        const { _id: donId } = await donModel.create(don);

        const necesidad = {
          name: 'aprender inglés',
          community: '6346055bf1d3ca76e73f01b5',
          categories: ['634c1976d1c1dead2de0a7dd'],
          location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
          users: ['6344766175c0ac1078cbc50d'],
          metadata: necesidadMetadataId,
          marker: ''
        }
        const { _id: necesidadId } = await necesidadModel.create(necesidad);

        // process
        const likeDones = [
          {
            name: dones[0].name,
            community: dones[0].community,
            categories: dones[0].categories,
            location: { type: 'Point', coordinates: dones[0].location.coordinates },
            users: [dones[0].users[0], don.users[0]],
            metadata: { dones: 2, necesidades: 2, descriptions: [dones[0].metadata.descriptions[0], donMetadata.descriptions[0]] },
            marker: '',
            __isNewMetadata: donMetadataId.toString(),
          }
        ];
        const likeNecesidades = [
          {
            name: necesidades[0].name,
            community: necesidades[0].community,
            categories: necesidades[0].categories,
            location: { type: 'Point', coordinates: necesidades[0].location.coordinates },
            users: [necesidades[0].users[0], necesidad.users[0]],
            metadata: { dones: 2, necesidades: 2, descriptions: [necesidades[0].metadata.descriptions[0], necesidadMetadata.descriptions[0]] },
            marker: '',
            __isNewMetadata: necesidadMetadataId.toString(),
          }
        ];

        const paDones = migrator.mixWithDBData(dones, 'dones');
        const resDones = await Promise.all(paDones);
        const paNecesidades = migrator.mixWithDBData(necesidades, 'necesidades');
        const resNecesidades = await Promise.all(paNecesidades);

        expect(resDones).toEqual(likeDones);
        expect(resNecesidades).toEqual(likeNecesidades);

        // after
        await donModel.deleteOne({ _id: donId.toString() });
        await necesidadModel.deleteOne({ _id: necesidadId.toString() });
        await donMetadataModel.deleteOne({ _id: donMetadataId.toString() });
        await donMetadataModel.deleteOne({ _id: necesidadMetadataId.toString() });
      })
    })

    // TODO: if one seed on bd, updates their dones with new users ??
    describe('[process] synomys with dictionary - [fn] processMetadata', () => {
      it('one synomyn don, create metadata', async () => {
        // before
        const { dones, necesidades } = testData.test19;

        // process
        const resDon = await migrator.processMetadata(dones, "dones");
        const resNecesidades = await migrator.processMetadata(necesidades, "necesidades");

        const metadataResDon = Array.from(new Set(resDon.map((r: any) => r.metadata.toString())));
        const metadataResNecesidades = Array.from(new Set(resNecesidades.map((r: any) => r.metadata.toString())));

        const phd = metadataResDon.map(async mrd => {
          const metadataDon = await donMetadataModel.find({ _id: mrd });
          expect(metadataDon.length).toEqual(1);
        })
        await Promise.all(phd);

        const phn = metadataResNecesidades.map(async mrn => {
          const metadataNecesidades = await necesidadMetadataModel.find({ _id: mrn });
          expect(metadataNecesidades.length).toEqual(1);
        })
        await Promise.all(phn);

        // after
        const pgd = metadataResDon.map(async m => {
          await donMetadataModel.deleteOne({ _id: m.toString() });
        })
        await Promise.all(pgd);
        const pgn = metadataResNecesidades.map(async m => {
          await necesidadMetadataModel.deleteOne({ _id: m.toString() });
        })
        await Promise.all(pgn);
      });
    })

    describe('[process] synomys with dictionary - use other synomyn - [fn] mixWithOpencalcData', () => {
      it('necesidad: eskutxado / don: eskutxar', async () => {
        let { dones, necesidades } = testData.test20;

        const likeDones = [
          {
            name: dones[0].name,
            community: dones[0].community,
            categories: dones[0].categories,
            location: { type: 'Point', coordinates: dones[0].location.coordinates },
            users: dones[0].users,
            metadata: { dones: 1, necesidades: 1, descriptions: dones[0].metadata.descriptions },
            marker: '',
          },
        ];

        const likeNecesidades = [
          {
            name: necesidades[0].name,
            community: necesidades[0].community,
            categories: necesidades[0].categories,
            location: { type: 'Point', coordinates: necesidades[0].location.coordinates },
            users: necesidades[0].users,
            metadata: { dones: 1, necesidades: 1, descriptions: necesidades[0].metadata.descriptions },
            marker: '',
          },
        ];

        dones = await migrator.indexByName(dones);
        necesidades = await migrator.indexByName(necesidades);

        const resDones = migrator.mixWithOpencalcData(dones, necesidades, 'dones');
        const resNecesidades = migrator.mixWithOpencalcData(necesidades, dones, 'necesidades');

        expect(resDones).toEqual(likeDones);
        expect(resNecesidades).toEqual(likeNecesidades);
      })
    })
  })
})
