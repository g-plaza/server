import { Necesidad } from '../../interfaces/necesidades.interface';

export const testLatLongs = {
  // Calle de Biteri, 2, 20120 Hernani
  "Hernani": ["43.266775", "-1.976021"],

  // Alameda del Boulevard, 18, 20003 San Sebastián
  "Donosti": ["43.322450", "-1.984600"],

  // Calle San Francisco Javier, 11, 20303 Irun
  "Irun": ["43.333940", "-1.791820"],

  // Zikuñaga Bailara, 49, 20120 Hernani
  "Alice": ["43.264080", "-1.968270"],

  // Plaza de Nestor Basterretxea, 20012 San Sebastián
  "Alici": ["43.317330", "-1.975820"],

  // Calle San Francisco Javier, 11, 20303 Irun
  "Aliceir": ["43.333940", "-1.791820"],

  // Zikuñaga Bailara, 49, 20120 Hernani
  "Bob": ["43.264080", "-1.968270"],
}

export const testData = {
  test1: {
    dones: [
      {
        A: 'limpiar comunidad de vecinos',
        B: 'lo',
        C: 'Zikuñaga Bailara, 49, 20120 Hernani',
        D: 'Hernani',
        E: 'Calle de Biteri, 2, 20120 Hernani',
        F: 'description1',
        G: 'Alice',
        H: 'telegram',
        I: '@alice',
        J: 'email',
        L: 'tfno'
      }
    ],
    necesidades: [
      // no deberia tener como username Alice, deberia de tener
      // Alice pero es para que pase el test de processUsers
      // '2 users => 1 unique user'
      {
        A: 'aprender inglés',
        B: 'lagunak',
        C: 'Plaza de Nestor Basterretxea, 20012 San Sebastián',
        D: 'Donosti',
        E: 'Alameda del Boulevard, 18, 20003 San Sebastián',
        F: 'Description-necesidad1',
        G: 'Alice',
        H: 'telegram',
        I: '@alice',
        J: 'email',
        L: 'tfno'
      }
    ]
  },
  test2: {
    dones: [
      {
        A: 'limpiar comunidad de vecinos',
        B: 'lo',
        C: 'Zikuñaga Bailara, 49, 20120 Hernani',
        D: 'Hernani',
        E: 'Calle de Biteri, 2, 20120 Hernani',
        F: 'dones-description1',
        G: 'Alice',
        H: 'telegram',
        I: '@alice',
        J: 'email',
        K: 'alice@email.com',
        L: 'tfno'
      },
      {
        A: 'limpiar comunidad de vecinos',
        B: 'lo',
        C: 'Zikuñaga Bailara, 49, 20120 Hernani',
        D: 'Hernani',
        E: 'Calle de Biteri, 2, 20120 Hernani',
        F: 'dones-description1',
        G: 'Bob',
        H: 'telegram',
        I: '@bob',
        J: 'email',
        K: 'bob@email.com',
        L: 'tfno',
        M: '611223344',
      }
    ],
    necesidades: []
  },
  test3: {
    dones: [],
    necesidades: [{
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Zikuñaga Bailara, 49, 20120 Hernani',
      D: 'Hernani',
      E: 'Calle de Biteri, 2, 20120 Hernani',
      F: 'dones-description1',
      G: 'Alice',
      H: 'telegram',
      I: '@alice',
      J: 'email',
      K: 'alice@email.com',
      L: 'tfno',
      M: '611998877',
      N: 'web',
      O: 'http://hi.com'
    },
    {
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Zikuñaga Bailara, 49, 20120 Hernani',
      D: 'Hernani',
      E: 'Calle de Biteri, 2, 20120 Hernani',
      F: 'dones-description1',
      G: 'Bob',
      H: 'telegram',
      I: '@bob',
      J: 'email',
      K: 'bob@email.com',
      L: 'tfno',
      M: '611223344',
      N: 'web',
      O: 'http://hello.com',
      P: 'another',
      Q: 'one'
    }]
  },
  test4: {
    dones: [{
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Plaza de Nestor Basterretxea, 20012 San Sebastián',
      D: 'Donosti',
      E: 'Alameda del Boulevard, 18, 20003 San Sebastián',
      F: 'dones-description1',
      G: 'Alici',
      H: 'telegram',
      I: '@alici',
      J: 'email',
      L: 'tfno'
    }],
    necesidades: [{
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Plaza de Nestor Basterretxea, 20012 San Sebastián',
      D: 'Donosti',
      E: 'Alameda del Boulevard, 18, 20003 San Sebastián',
      F: 'dones-description1',
      G: 'Alici',
      H: 'telegram',
      I: '@alici',
      J: 'email',
      L: 'tfno'
    },
    {
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Zikuñaga Bailara, 49, 20120 Hernani',
      D: 'Hernani',
      E: 'Calle de Biteri, 2, 20120 Hernani',
      F: 'dones-description2',
      G: 'Bob',
      H: 'telegram',
      I: '@bob',
      J: 'email',
      L: 'tfno'
    }]
  },
  test5: {
    dones: [{
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Calle de Karobiaga, 22, 20305 Irun',
      D: 'Irun',
      E: 'Calle San Francisco Javier, 11, 20303 Irun',
      F: 'dones-description1',
      G: 'Aliceir',
      H: 'telegram',
      I: '@aliceir',
      J: 'email',
      L: 'tfno'
    }],
    necesidades: [{
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Plaza de Nestor Basterretxea, 20012 San Sebastián',
      D: 'Donosti',
      E: 'Alameda del Boulevard, 18, 20003 San Sebastián',
      F: 'necesidades-description1',
      G: 'Alici',
      H: 'telegram',
      I: '@alici',
      J: 'email',
      L: 'tfno'
    },
    {
      A: 'limpiar comunidad de vecinos',
      B: 'lo',
      C: 'Zikuñaga Bailara, 49, 20120 Hernani',
      D: 'Hernani',
      E: 'Calle de Biteri, 2, 20120 Hernani',
      F: 'necesidades-description2',
      G: 'Bob',
      H: 'telegram',
      I: '@bob',
      J: 'email',
      L: 'tfno'
    }]
  },
  test6: {
    dones: [],
    necesidades: []
  },
  test7: {
    dones: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description1"] },
      marker: ''
    }],
    necesidades: [{
      name: 'pasear perros',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["Description-necesidad1"] },
      marker: ''
    }]
  },
  test8: {
    dones: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description1"] },
      marker: ''
    }],
    necesidades: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["Description-necesidad1"] },
      marker: ''
    }]
  },
  test9: {
    dones: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description1"] },
      marker: ''
    },
    {
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["1", "2"] },
      users: ['6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description2"] },
      marker: ''
    },
    {
      name: 'limpiar comunidad de vecinos',
      community: '634c1976d1c1dead2de0a7dd',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["10", "11"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description3"] },
      marker: ''
    }],
    necesidades: [{
      name: 'otra necesidad',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["Description-necesidad1"] },
      marker: ''
    }]
  },
  test10: {
    dones: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description1"] },
      marker: ''
    },
    {
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["1", "2"] },
      users: ['6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description2"] },
      marker: ''
    },
    {
      name: 'limpiar comunidad de vecinos',
      community: '634c1976d1c1dead2de0a7dd',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["10", "11"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description3"] },
      marker: ''
    }],
    necesidades: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["Description-necesidad1"] },
      marker: ''
    }]
  },
  test11: {
    dones: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description1"] },
      marker: ''
    },
    {
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["1", "2"] },
      users: ['6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["description2"] },
      marker: ''
    }],
    necesidades: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["Description-necesidad1"] },
      marker: ''
    }]
  },
  test12: {
    dones: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7', '6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 2, necesidades: 1, descriptions: ["description1", "description2"] },
      marker: ''
    },
    {
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["1", "2"] },
      users: ['6346055bf1d3ca76e73f01b7', '6346055cf1d3ca76e73f01bc'],
      metadata: { dones: 2, necesidades: 1, descriptions: ["description1", "description2"] },
      marker: ''
    }],
    necesidades: [{
      name: 'limpiar comunidad de vecinos',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["Description-necesidad1"] },
      marker: ''
    }]
  },
  test13: {
    // crear antes un metadata para dones[0]

    /* {
        "necesidades" : 0,
        "dones" : 1,
        "descriptions" : [
            "description1"
        ]
    } */
    dones: [
      {
        name: 'limpiar comunidad de vecinos',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            43.3172119368173,
            -1.97544130496173
          ]
        },
        users: ['6346055bf1d3ca76e73f01b7', '6346055bf1d3ca76e73f01b9'],
        metadata: { dones: 2, necesidades: 0, descriptions: ["description1", "description2"] },
        __isNewMetadata: true,
        marker: ''
      },
      {
        name: 'limpiar comunidad de vecinos',
        community: '6349ea93d1c1dead2d520164',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            1,
            2
          ]
        },
        users: ['6346055cf1d3ca76e73f01bc'],
        metadata: { dones: 1, necesidades: 0, descriptions: ["description11"] },
        __isNewMetadata: true,
        marker: ''
      },
      {
        name: '(des)carga mercados',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            3,
            4
          ]
        },
        users: ['6344766175c0ac1078cbc512'],
        metadata: { dones: 1, necesidades: 0, descriptions: ["description100"] },
        __isNewMetadata: true,
        marker: ''
      },
      {
        name: 'limpiar comunidad de vecinos',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            5,
            6
          ]
        },
        users: [
          '6346055bf1d3ca76e73f01b7',
          '6346055bf1d3ca76e73f01b9'
        ],
        metadata: { dones: 2, necesidades: 0, descriptions: ["description1", "description2"] },
        __isNewMetadata: true,
        marker: ''
      }
    ],
    necesidades: []
  },
  test14: {
    // crear antes un metadata para dones[0]

    /* {
        "necesidades" : 0,
        "dones" : 1,
        "descriptions" : [
            "description1"
        ]
    } */
    dones: [
      {
        name: 'limpiar comunidad de vecinos',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            43.3172119368173,
            -1.97544130496173
          ]
        },
        users: ['6346055bf1d3ca76e73f01b7', '6346055bf1d3ca76e73f01b9'],
        metadata: "6353265d99deb2a5307a76fe",
        __isNewMetadata: "6353265d99deb2a5307a76fe",
        marker: ''
      },
      {
        name: 'limpiar comunidad de vecinos',
        community: '6349ea93d1c1dead2d520164',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            1,
            2
          ]
        },
        users: ['6346055cf1d3ca76e73f01bc'],
        metadata: "6353265d99deb2a5307a7700",
        __isNewMetadata: "6353265d99deb2a5307a7700",
        marker: ''
      },
      {
        name: '(des)carga mercados',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            3,
            4
          ]
        },
        users: ['6344766175c0ac1078cbc512'],
        metadata: "6353265d99deb2a5307a76ff",
        __isNewMetadata: "6353265d99deb2a5307a76ff",
        marker: ''
      },
      {
        name: 'limpiar comunidad de vecinos',
        community: '6346055bf1d3ca76e73f01b5',
        categories: ['6349eb2bd1c1dead2d522ae5'],
        location: {
          type: 'Point', coordinates: [
            5,
            6
          ]
        },
        users: [
          '6346055bf1d3ca76e73f01b7',
          '6346055bf1d3ca76e73f01b9'
        ],
        metadata: "6353265d99deb2a5307a76fe",
        __isNewMetadata: "6353265d99deb2a5307a76fe",
        marker: ''
      }
    ],
    necesidades: []
  },
  test15: {
    dones: [],
    necesidades: [{
      name: 'pasear perros',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: "6353265d99deb2a5307a7700",
      __isNewMetadata: "6353265d99deb2a5307a7700",
      marker: ''
    }]
  },
  test16: {
    dones: [],
    necesidades: [{
      name: 'huerting',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: "635c41a9d1c1dead2d094bea",
      __isNewMetadata: "635c41a9d1c1dead2d094bea",
      marker: ''
    },
    {
      name: 'bricomanía',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: "635c41bdd1c1dead2d0950e0",
      __isNewMetadata: "635c41bdd1c1dead2d0950e0",
      marker: ''
    },
    {
      name: 'jardinería',
      community: '6349ea93d1c1dead2d520164',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055cf1d3ca76e73f01bc'],
      metadata: "635c41f2d1c1dead2d095d7a",
      __isNewMetadata: "635c41f2d1c1dead2d095d7a",
      marker: ''
    }]
  },
  test17: {
    dones: [{
      name: 'enseñar inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["enseñaringles"] },
      marker: ''
    }],
    necesidades: [{
      name: 'aprender inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["aprenderinglés"] },
      marker: ''
    }]
  },
  test18: {
    dones: [{
      name: 'enseñar inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["enseñaringles1"] },
      marker: ''
    },
    {
      name: 'enseñar inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6344766175c0ac1078cbc50d'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["enseñaringles2"] },
      marker: ''
    }],
    necesidades: [{
      name: 'aprender inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["aprenderinglés1"] },
      marker: ''
    },
    {
      name: 'aprender inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6344766175c0ac1078cbc510'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["aprenderinglés1"] },
      marker: ''
    }]
  },
  test19: {
    dones: [{
      name: 'enseñar inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["enseñaringles"] },
      __isNewMetadata: true,
      marker: ''
    }],
    necesidades: [{
      name: 'aprender inglés',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["aprenderinglés"] },
      __isNewMetadata: true,
      marker: ''
    }]
  },
  test20: {
    dones: [{
      name: 'eskutxar por quien lo necesite',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['6349eb2bd1c1dead2d522ae5'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b7'],
      metadata: { dones: 1, necesidades: 0, descriptions: ["enkutxaraquienlonecesite"] },
      marker: ''
    }],
    necesidades: [{
      name: 'eskutxado por quien lo necesite',
      community: '6346055bf1d3ca76e73f01b5',
      categories: ['634c1976d1c1dead2de0a7dd'],
      location: { type: 'Point', coordinates: ["43.3172119368173", "-1.97544130496173"] },
      users: ['6346055bf1d3ca76e73f01b9'],
      metadata: { dones: 0, necesidades: 1, descriptions: ["sereskutxadoporalguien"] },
      marker: ''
    }]
  },
  testN: {
    dones: [
      {
        A: 'limpiar comunidad de vecinos',
        B: 'lo',
        C: '43.3172119368173',
        D: '-1.97544130496173',
        E: 'Donosti',
        F: '43.3418969',
        G: '-1.8073705',
        H: 'description1',
        I: 'Egoitz',
        J: 'telegram',
        K: '@egoitz',
        L: 'email',
        N: 'tfno'
      },
      {
        A: '(des)carga mercados',
        B: 'lagunak',
        C: '43.3172119368173',
        D: '-1.97544130496173',
        E: 'Donosti',
        F: '43.3418970',
        G: '-1.8073706',
        H: 'description2',
        I: 'Egoitz',
        J: 'telegram',
        K: '@egoitz',
        L: 'email',
        N: 'tfno'
      },
      {
        A: 'limpiar comunidad de vecinos',
        B: 'lo',
        C: 1,
        D: 2,
        E: 'Donosti',
        F: '43.3418969',
        G: '-1.8073705',
        H: 'description3',
        I: 'Paco',
        J: 'telegram',
        K: '@paco',
        L: 'email',
        N: 'tfno'
      }
    ],
    necesidades: [
      {
        A: 'aprender inglés',
        B: 'lagunak',
        C: '43.3172119368173',
        D: '-1.97544130496173',
        E: 'Donosti',
        F: '43.3418968',
        G: '-1.8073704',
        H: 'Description-necesidad1',
        I: 'Egoitz',
        J: 'telegram',
        K: '@egoitz',
        L: 'email',
        N: 'tfno'
      },
      {
        A: 'limpiar comunidad de vecinos',
        B: 'lo',
        C: '43.3226904864312',
        D: '-1.98216234959986',
        E: 'Donosti',
        F: '43.3418969',
        G: '-1.8073705',
        H: 'Description-necesidad2',
        I: 'Maria',
        J: 'telegram',
        K: '@maria',
        L: 'email',
        N: 'tfno'
      }
    ]
  }
}
