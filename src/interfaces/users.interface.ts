export interface User {
  _id: string;
  name: string;
  username: string;
  email: string;
  center: string;
  contact_ways: Object;
}
