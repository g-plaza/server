export interface Community {
  _id: string;
  name: string;
  center: string;
}
