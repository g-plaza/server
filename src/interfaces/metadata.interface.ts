export interface Metadata {
  _id: string;
  necesidades: number;
  dones: number;
  /* locations: any[]; */
  descriptions: string[];
}
