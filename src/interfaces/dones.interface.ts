import { Category } from './category.interface';
import { Community } from './community.interface';
import { Metadata } from './metadata.interface';
import { User } from './users.interface';

export interface Don {
  _id: string;
  name: string;
  community: Community;
  categories: Category[];
  location: any;
  users: User[];
  position: number;
  metadata: Metadata;
}
