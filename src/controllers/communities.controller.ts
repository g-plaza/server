import { NextFunction, Request, Response } from 'express';
import { Community } from '@interfaces/community.interface';
import communityService from '@services/community.service';


class CommunitiesController {
  public communityService = new communityService();

  public getCommunities = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const allCommunities: any = await this.communityService.findAllCommunities();
      res.status(200).json({ data: allCommunities, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

}

export default CommunitiesController;
