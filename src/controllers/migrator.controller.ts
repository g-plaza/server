// desde raiz del proyecto:
// npm run build && node src/controllers/migrator.controller.js / npm run migrator
// ./node_modules/jest-cli/bin/jest.js --forceExit src/tests/migrator.test.ts
import { connect } from 'mongoose';
import communityModel from '../models/community.model';

class Migrator {
  dones: any;

  constructor() {
    this.new();
  }

  async new() {
    const { DB_USER, DB_PASS, DB_HOST, DB_DATABASE } = process.env;
    await connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_DATABASE}`);
  }

  async allDones() {
    this.dones = await communityModel.find();
  }
}

async function main() {
  const migrator = new Migrator();
  await migrator.allDones();
  console.log('end', migrator.dones);
  return true;
}

main();

/*
const excelToJson = require('convert-excel-to-json');
// import excelToJson from 'convert-excel-to-json';
import { config } from 'dotenv';
config({ path: `.env.${process.env.NODE_ENV || 'development'}.local` });
/* import communityModel from '@models/community.model';
import { Community } from '@interfaces/community.interface'; */
/* import communityModel from '../../dist/models/community.model';

// console.log("env", process.env.NODE_ENV, process.env)

async function main() {

    /* let communities: Community[] = await communityModel.find();
    console.log("communities", communities) */

/* const jsonData = dataToJson();

// 1. start with dones
let type = "dones";
// 1.1 process comunities
const pa = processCommunities(jsonData.dones);
const communitiesData = await Promise.all(pa);*/
// 1.2 process dones itself
// 1.2.1 decorate seed
/* const pa = decorateSeed(jsonData.dones, type);
const donesData = await Promise.all(pa);

donesData.map(don => {
    console.log("don", don);

    // busca si hay algun don con ese nombre en Dons
    // ...
}) */
/* }

main();

function dataToJson() {
    const jsonData = excelToJson({
        sourceFile: './src/migrator/data/migrationPrueba1.ods'
    });
    // console.log("result", result)
    delete jsonData.necesidades[0];
    delete jsonData.dones[0];

    return jsonData;
}

function decorateSeed(data, type: string) {
    return data.map(async d => {
        const community = d["E"];
        const description = d["F"];
        const name = d["A"];
        // TODO: trim y rtrim para quitar los espacios delante y detras
        // TODO: entran en minusculas y sin acentos.
        let categories = d["B"];
        let necesidades = 0;
        let dones = 0;

        let users = [{
            name: d["G"],
            contact_ways: []
        }];

        let locations = [{
            type: "Point",
            coordinates: [
                parseFloat(d["C"]),
                parseFloat(d["D"])
            ]
        }]
        categories = categories.split(",");

        if (type === "necesidades") {
            necesidades = necesidades + 1;
        } else if (type === "dones") {
            dones = dones + 1;
        }
        // console.log("users final", users)


        if (d["G"] !== undefined && d["H"] !== undefined) {
            users[users.length - 1].contact_ways.push({
                type: d["G"],
                data: d["H"]
            })
        }
        if (d["I"] !== undefined && d["J"] !== undefined) {
            users[users.length - 1].contact_ways.push({
                type: d["I"],
                data: d["J"]
            })
        }

        if (d["K"] !== undefined && d["L"] !== undefined) {
            users[users.length - 1].contact_ways.push({
                type: d["K"],
                data: d["L"]
            })
        }

        const resp = {
            name,
            description,
            necesidades,
            dones,
            categories,
            locations,
            marker: "",
            community,
            users
        };

        return resp;
    })
}

function processCommunities(data) {
    // console.log("data", data)
    return data.map(async d => {
        console.log("entran", d)
        const community = d["E"];

        // buscar si existe ese nombre de comunidad.
        let communities: any[] = await communityModel.find();
        console.log("communities", communities)
        //// sino existe crearlo en Comminities
    })
}

 */
