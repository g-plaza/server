import { NextFunction, request, Request, Response } from 'express';
import { Don } from '@/interfaces/dones.interface';
import communityModel from '@/models/community.model';
import DonesService from '@/services/dones.service';

class DonesController {
  public donService = new DonesService();
  public getDones = async (req: Request, res: Response, next: NextFunction) => {
    // console.log('COMMUNITY', await communityModel.find());
    // console.log('inicioa', req.body, req.body.location);
    try {
      console.log('entra');
      // curl -X 'POST' 'http://localhost:3000/dones' -H 'accept: application/json' -H "Content-Type: application/json" -d '{"hola": "hola"}'

      // body:
      /* {
                "type": "dones",
                "community": "63309ce817d6733c900c8368",
                "location": {
                    "geometry":[43.3170546,-1.9775704],
                    "maxDistance": 0
                },
                // "text": "string"
                }
            } */
      const type = req.body['type'];
      const categories = req.body['categories'] || [];
      const community = req.body['community'] || null;
      const freeText = req.body['text'] || undefined;
      let maxDistance = undefined;
      if (req.body['location'] !== undefined) {
        maxDistance = req.body['location']['maxDistance'];
      }
      const search: any = {};

      console.log('DATA!', type, categories, community, freeText, maxDistance);

      if (community != null) {
        search.community = {};

        if (typeof community === 'string') {
          search.community = { $in: [community] };
        } else {
          search.community = { $in: community };
        }
      }

      if (categories.length > 0) {
        search.categories = {};

        if (typeof categories === 'string') {
          search.categories = { $in: [categories] };
        } else {
          search.categories = { $in: categories };
        }
      }

      // console.log("`\"casero\"`", `\`\"${freeText}\"\``, freeText)
      if (freeText != undefined && freeText != '') {
        console.log('hace busqueda');
        search['$text'] = {};
        search['$text'] = { $search: `\`\"${freeText}\"\`` };
        // hasFreeTextFilled = true;
      }

      if (maxDistance != undefined && maxDistance !== 0) {
        console.log('dinstance', type, categories, community, freeText, maxDistance, req.body['text'], typeof community);
        console.log('entra en distance');

        // delete search["community"];

        // si tiene freeText:
        // evitar: "text and geoNear not allowed in same query"
        if (freeText != undefined && freeText != '') {
          console.log('tiene free text. do nothing');

          delete search['$text'];
        }

        const geometry = req.body['location']['geometry'];
        console.log('geometry', geometry);

        search['location'] = {};
        search['location']['$near'] = {};
        search['location']['$near']['$geometry'] = { type: 'Point', coordinates: [parseFloat(geometry[0]), parseFloat(geometry[1])] };
        search['location']['$near']['$minDistance'] = 1;
        search['location']['$near']['$maxDistance'] = maxDistance;
        // hasFreeTextFilled = true;
      }
      console.log('SEARCH on dones controller', search);

      const findAllDonesData: Don[] = await this.donService.findAllDones(search, freeText, type);

      res.status(200).json({ data: findAllDonesData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };
}

export default DonesController;
