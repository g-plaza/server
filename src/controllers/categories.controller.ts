import { NextFunction, Request, Response } from 'express';
import { Category } from '@interfaces/category.interface';
import categoryService from '@services/categories.service';

export type CategoryObj<T> = {
  [K in keyof T]: { _id: K, name: T[K] }
}[keyof T]

class CategoriesController {
  public categoryService = new categoryService();

  public getCategories = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const allCategories: CategoryObj<Category> = await this.categoryService.findAllCategories();
      res.status(200).json({ data: allCategories, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

}

export default CategoriesController;
