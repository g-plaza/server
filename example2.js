// Requiring module
const mongoose = require('mongoose');

// Connecting to database
mongoose.connect('mongodb+srv://janlolagunak:STshxcuMCphHDdJH@cluster0.bgywnkb.mongodb.net/v1prod?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

// Creating Schemas
const donSchema = new mongoose.Schema({
  name: String,
  /* necesidades: Number,
  dones: Number, */
  community: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Community'
  },
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
})

const necesidadSchema = new mongoose.Schema({
  name: String,
 /*  necesidades: Number,
  dones: Number, */
  community: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Community'
  },
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
})

const communitySchema = new mongoose.Schema({
  name: String,
  center: String,
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  }
})

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    // required: true,
    unique: true,
  },
  email: String,
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  }
})

const categorySchema = new mongoose.Schema({
  name: String,
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date,
    required: false,
  }
})

// Creating and registering models from donSchema and communitySchema
const Don = mongoose.model('Don', donSchema);
const Necesidad = mongoose.model('Necesidad', necesidadSchema);
const Community = mongoose.model('Community', communitySchema);
const User = mongoose.model('User', userSchema);
const Category = mongoose.model('Category', categorySchema);

// lagunak 633315a0973160bf2ee39fad
// jan 633315ae973160bf2ee3a3b0
// lo 633315b6973160bf2ee3a516


// irun 6369758d7287ab5b434817a4
// zamudio 6369758d7287ab5b43481790
const search = {
  type: "necesidades",
  // categories: { $in: ["633315ae973160bf2ee3a3b0"] },
  /* $text: { $search: "\"casero\"" } */

  // community: mongoose.Types.ObjectId("6369758d7287ab5b43481790"),
  // community: ["6369758d7287ab5b43481790"],
   community: { '$in': [ '6369758d7287ab5b43481790' ] }
}

Necesidad.find(search).then(p => console.log("ASDASD", p))
// console.log("ASDASD", a)
// Query to find and show all the Dons
Don.find(search)
  .populate("community")
  .populate("users")
  .populate("categories")
  .then(p => console.log(p))
  // .then(p => console.log(p[0].categories[0].name))
  // .then(p => console.log(p[0].users[0].username))
  // .then(p => console.log(p[0].community.name))
  .catch(error => console.log(error));
